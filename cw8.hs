module Mymodules  
( mysubset  
, myintersection  
, mymember  
, myunion  
, mydifference
) where 

import Data.List

mysubset :: Eq a => [a] -> [a] -> [Char]
mysubset [] _ = "TRUE"
mysubset (x:xs)(listY) = if  x `elem` listY then mysubset xs listY else "FALSE"

myintersection :: Eq a => [a] -> [a] -> [a]
myintersection [] _ = []
myintersection (x:xs)(listY) = if  x `elem` listY then x:(myintersection xs listY) else myintersection xs listY

mymember :: Eq a => a -> [a] -> [Char]
mymember x [] = "FALSE"
mymember x (y:ys) = if  x == y then "TRUE" else mymember x ys

myunion :: [a] -> [a] -> [a]
myunion [] [] = []
myunion listX [] = listX
myunion [] listY = listY
myunion (listX) (listY) = listX ++ listY

mydifference :: Eq a => [a] -> [a] -> [a]
mydifference [] _ = []
mydifference listX [] = listX
--mydifference (listX) (listY) = listX \\ listY
mydifference (x:xs) listY = if x `elem` listY then mydifference xs listY else x:mydifference xs listY